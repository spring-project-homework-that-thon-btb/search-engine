package search.engine.searchengine.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import search.engine.searchengine.models.Student;

@Repository
public interface StudentServiceTest extends CrudRepository<Student,Integer> {
}

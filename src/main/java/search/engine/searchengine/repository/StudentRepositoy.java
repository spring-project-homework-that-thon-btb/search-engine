package search.engine.searchengine.repository;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.stereotype.Repository;
import search.engine.searchengine.models.Student;

import java.util.List;

@Repository
public interface StudentRepositoy extends ElasticsearchCrudRepository<Student,Integer> {

    List<Student> findByName(String name);

    List<Student> getAllBy();

}

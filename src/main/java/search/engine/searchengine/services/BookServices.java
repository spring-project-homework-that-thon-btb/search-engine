package search.engine.searchengine.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import search.engine.searchengine.models.Student;
import search.engine.searchengine.repository.StudentRepositoy;

import java.util.List;

@Service
public class BookServices {
    @Autowired
    private StudentRepositoy studentRepositoy;

    public List<Student> findStudentByName(String name){
        return studentRepositoy.findByName(name);
    }
}

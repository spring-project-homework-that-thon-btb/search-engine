package search.engine.searchengine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import search.engine.searchengine.models.Student;
import search.engine.searchengine.repository.StudentRepositoy;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@Transactional
public class SearchEngineApplication implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(SearchEngineApplication.class, args);
    }


    @Autowired
    private StudentRepositoy studentRepositoy;

    @Autowired
    private ElasticsearchOperations elasticsearchOperations;

    @Override
    public void run(ApplicationArguments args) {
//        elasticsearchOperations.putMapping(Student.class);
        System.out.println("Loading Data");
//
        List<Student> students = new ArrayList<>();
        students.add(new Student("Nora", "Male", "PP"));
        students.add(new Student("Sida", "Female", "AA"));
        students.add(new Student("Keo", "Male", "SS"));
        students.add(new Student("Koa", "Female", "BB"));
        studentRepositoy.saveAll(students);
    }
}


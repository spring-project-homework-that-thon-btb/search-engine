package search.engine.searchengine.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import search.engine.searchengine.models.Student;
import search.engine.searchengine.repository.StudentRepositoy;
import search.engine.searchengine.repository.StudentServiceTest;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/search")
public class StudentController {
    @Autowired
    private StudentRepositoy studentRepositoy;

    @GetMapping("/{name}")
    public List<Student> allStudent(@PathVariable("name") String name){
        return studentRepositoy.findByName(name);
    }

    @GetMapping("/index")
    public List<Student> index(){
        return studentRepositoy.getAllBy();
    }

    @GetMapping("/id/{id}")
    public Optional<Student> findById(@PathVariable("id") Integer id){
        return studentRepositoy.findById(id);
    }


    @Autowired
    private StudentServiceTest serviceTest;

    @GetMapping("/")
    public Iterable<Student> all(){
        return serviceTest.findAll();
    }
}
